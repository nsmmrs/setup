# Add frequently-accessed directories to global cd auto-completion
# ( entries delimited by colon )
export CDPATH+=":~"

# IMPROVED SORTING FOR LS, ETC.
export LC_COLLATE=C

# DEFAULT APPLICATIONS
export SETUP_EDITOR="kak"
export EDITOR=${SETUP_EDITOR}
export VISUAL=${SETUP_EDITOR}
export SVN_EDITOR=${SETUP_EDITOR}
export GIT_EDITOR=${SETUP_EDITOR}

# FZF DEFAULT COMMAND
# --files: List files that would be searched but do not search
# --no-ignore: Do not respect .gitignore, etc...
# --hidden: Search hidden files and folders
# --follow: Follow symlinks
# --glob: Additional conditions for search (in this case ignore everything in the .git/ folder)
export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow --glob "!.git/*"'
