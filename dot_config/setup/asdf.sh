setup_asdf () {
  if missing asdf ; then
    source $HOME/.asdf/asdf.sh

    if [ -n $BASH_VERSION ]; then
      source $HOME/.asdf/completions/asdf.bash
    elif [ -n $ZSH_VERSION ]; then
      fpath=(${ASDF_DIR}/completions $fpath)
      autoload -Uz compinit && compinit
    fi
  fi
}

setup_asdf
