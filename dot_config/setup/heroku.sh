if missing heroku ; then
  not_found "heroku cli"
else
  production () {
  if [[ $# -gt 0 ]]; then
    heroku "$@" --remote production
  else
    heroku help
  fi
  }

  staging () {
  if [[ $# -gt 0 ]]; then
    heroku "$@" --remote staging
  else
    heroku help
  fi
  }
fi
