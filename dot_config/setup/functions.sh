on_mac () {
  [[ $OSTYPE == *"darwin"* ]]
}

on_linux () {
  [[ $OSTYPE == *"linux"* ]]
}

missing () {
  command -v $1 &>/dev/null

  test $? -ne 0
}

not_found () {
  echo "WARNING: ${1} not found!"
}

reload_setup () {
  source ${HOME}/.profile
}
