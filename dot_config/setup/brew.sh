setup_brew () {

  local BREW_SHELLENV=${HOME}/.config/setup/brew_shellenv.sh

  test -r ${BREW_SHELLENV} && source ${BREW_SHELLENV}

  if missing brew ; then
    local POTENTIAL_BREW_LOCATIONS=(
      "/usr/local/bin/brew"
      "/home/linuxbrew/.linuxbrew/bin/brew"
      "${HOME}/.linuxbrew/bin/brew"
    )

    for BREW in "${POTENTIAL_BREW_LOCATIONS[@]}"; do
      if [ ${BREW} ] && [ -x ${BREW} ]; then
        echo "caching 'brew shellenv' output in ${BREW_SHELLENV}..."

        ${BREW} shellenv >| ${BREW_SHELLENV} && source ${BREW_SHELLENV}

        break
      fi
    done

    test -r ${BREW_SHELLENV} && source ${BREW_SHELLENV}
  fi

  if missing brew ; then
    echo "couldn't find brew executable"
  elif on_mac ; then
    export RUBY_CONFIGURE_OPTS="--with-readline-dir=${HOMEBREW_PREFIX}/opt/readline"
    export PKG_CONFIG_PATH="${PKG_CONFIG_PATH:+${PKG_CONFIG_PATH}:}${HOMEBREW_PREFIX}/opt/openssl/lib/pkgconfig"
  fi
}

setup_brew
