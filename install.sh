# source helper functions
source <( curl -fsSL https://gitlab.com/noahsummers/setup/raw/master/dot_config/setup/functions.sh )
source <( curl -fsSL https://gitlab.com/noahsummers/setup/raw/master/install/steps.sh )

if missing git ; then
  brew install git
fi

command -v git && \
setup_install_brew && \
setup_install_asdf && \
setup_install_heroku && \
setup_install_bash && \
setup_install_chezmoi && \
setup_install_tools && \
echo "\n\aDone!" && \
echo "\nRun 'chezmoi diff' to see pending configuration changes." && \
echo "Run 'chezmoi apply' to apply them." && \
echo "\nRestart your terminal emulator to use your new setup!"
